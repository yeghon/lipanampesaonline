package lipaNaMpesa.stkPush;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import lipaNaMpesa.Autgenticate.Authenticate;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class STKPush 
{
	@Autowired
	public Authenticate authenticate;
	
	public String STKPushSimulation(
			String businessShortCode, //174379 
			String password,  // MTc0Mzc5YmZiMjc5ZjlhYTliZGJjZjE1OGU5N2RkNzFhNDY3Y2QyZTBjODkzMDU5YjEwZjc4ZTZiNzJhZGExZWQyYzkxOTIwMTkwNDIwMTcyNzUy
			String timestamp, //20190420172752
			String transactionType,  //CustomerPayBillOnline
			String amount, //1
			String phoneNumber, //254728626789
			String partyA, //254728626789
			String partyB, //174379
			String callBackURL, //http://mpesa-requestbin.herokuapp.com/16uny4w1
			//String queueTimeOutURL,
			String accountReference, // "Mtugo Meals"
			String transactionDesc) // "Pay Meal"
					throws Exception 
	{
		JSONArray jsonarray = new JSONArray();
		JSONObject jsonobject = new JSONObject();
		
		jsonobject.put("BusinessShortCode", businessShortCode);
        jsonobject.put("Password", password);
        jsonobject.put("Timestamp", timestamp);
        jsonobject.put("TransactionType", transactionType);
        jsonobject.put("Amount",amount);
        jsonobject.put("PhoneNumber", phoneNumber);
        jsonobject.put("PartyA", partyA);
        jsonobject.put("PartyB", partyB);
        jsonobject.put("CallBackURL", callBackURL);
        jsonobject.put("AccountReference", accountReference);
        //jsonobject.put("QueueTimeOutURL", queueTimeOutURL);
        jsonobject.put("TransactionDesc", transactionDesc);
        
        jsonarray.put(jsonobject);
        
        String requestJson = jsonobject.toString().replaceAll("[\\\\[\\\\]]", ""); 
        OkHttpClient client = new OkHttpClient();
        String url = "https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest";
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, requestJson);
        Request request = new Request.Builder()
        		.url(url)
        		.post(body)
        		.addHeader("content-type", "application/json")
        		.addHeader("authorization", "Bearer" + authenticate)
        		.addHeader("cache-control", "no-cache")
        		.build();
        
        Response response = client.newCall(request).execute();
        System.out.println(response.body().string());
        return response.body().toString();
        
    }

	
}
