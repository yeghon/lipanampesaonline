package lipaNaMpesa.Autgenticate;

import java.util.Base64;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@Component
public class Authenticate 
{
	public String authenticate() throws Exception
	{
		String app_key = null;
		String app_secret = null;
		
		String appKeySecret = app_key + ":" + app_secret;
        byte[] bytes = appKeySecret.getBytes("ISO-8859-1");
        String encoded = Base64.getEncoder().encodeToString(bytes);
        
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
        		.url("https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials")
        		.get()
        		.addHeader("authorization", "Basic" + encoded)
        		.addHeader("cache-control", "no-cache")
        		.build();
        
        Response response = client.newCall(request).execute();
        JSONObject jsonObject = new JSONObject(response.body().string());
        
        return jsonObject.getString("access_token");
	}
}
	

/*
      
        System.out.println(jsonObject.getString("access_token"));
        return jsonObject.getString("access_token");
    }

}
*/